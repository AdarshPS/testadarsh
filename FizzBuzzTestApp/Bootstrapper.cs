// <copyright file="Bootstrapper.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// Bootstrapper class helps in constructor dependency injection
namespace FizzBuzzTestApp
{
    using System.Web.Mvc;
    using BusinessLayer;
    using BusinessLayer.Interfaces;
    using Microsoft.Practices.Unity;
    using Unity.Mvc3;
    
    /// <summary>
    /// This class helps in dependency injection
    /// </summary>
    public static class Bootstrapper
    {
        /// <summary>
        /// This method will initialize the container to resolve the dependency
        /// </summary>
        public static void Initialise()
        {
            var container = BuildUnityContainer();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        /// <summary>
        /// This method contains all the interfaces and class for register
        /// </summary>
        /// <returns>container</returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();     
            container.RegisterType<ICommonDivisibleInterface, DivisibleByThreeAndFiveLogic>("DivisibleByThreeAndFiveLogic");
            container.RegisterType<ICommonDivisibleInterface, DivisibleByThreeLogic>("DivisibleByThreeLogic");
            container.RegisterType<ICommonDivisibleInterface, DivisibleByFiveLogic>("DivisibleByFiveLogic");
            container.RegisterType<IGetFizzBuzzPattern, GetFizzBuzzPattern>();
            return container;
        }
    }
}