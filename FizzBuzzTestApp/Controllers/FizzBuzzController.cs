﻿// <copyright file="FizzBuzzController.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This controller Class contains a logic to call the Business layer
//// The first request will come to this controller 
namespace FizzBuzzTestApp.Controllers
{
    using System;
    using System.Web.Mvc;
    using BusinessLayer.Interfaces;
    using Models;

    /// <summary>
    /// This Class contains a logic to call the Business layer
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Read only property for interface helps in constructor injection
        /// </summary>
        private readonly IGetFizzBuzzPattern igetFizzBuzzPattern;

        /// <summary>
        /// Constructor Injection
        /// </summary>
        /// <param name="getFizzBuzzPattern">getFizzBuzzPattern</param>
        public FizzBuzzController(IGetFizzBuzzPattern getFizzBuzzPattern)
        {
            this.igetFizzBuzzPattern = getFizzBuzzPattern;
        }

        /// <summary>
        /// This method accepts input number as parameter
        /// </summary>
        /// <param name="number">user input number</param>
        /// <returns>view with list of numbers</returns>
        [HttpGet]
        public ActionResult UserInput(string number)
        {
            var model = new FizzBuzzModel();
            var result = this.igetFizzBuzzPattern.GetFizzBuzzPatterns(Convert.ToInt32(number));
            model.LstNumbers = result;
            return View(model);
        }
    }
}
