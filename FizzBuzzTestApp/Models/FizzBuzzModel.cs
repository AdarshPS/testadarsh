﻿// <copyright file="FizzBuzzModel.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// Model contains a property to store the results
namespace FizzBuzzTestApp.Models
{
    using System.Collections.Generic;
    
    /// <summary>
    /// This class contains a property for the user inputted number
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets and sets property is used to store the results.
        /// </summary>
        public List<string> LstNumbers { get; set; }
    }
}