﻿// <copyright file="FilterConfig.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class helps in filtering concept
namespace FizzBuzzTestApp
{
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Filter configure class
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// This method helps in filtering concept
        /// </summary>
        /// <param name="filters">filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}