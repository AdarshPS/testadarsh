﻿// <copyright file="WebApiConfig.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This classhelps is web API concept
namespace FizzBuzzTestApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    /// <summary>
    /// web application program interface config
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// This method helps in configuring the default controller
        /// </summary>
        /// <param name="config">config</param>
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }
    }
}
