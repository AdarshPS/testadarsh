﻿// <copyright file="RouteConfig.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This config class helps in routing 
namespace FizzBuzzTestApp
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Route config file
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// This method is used to configuring the routing
        /// </summary>
        /// <param name="routes">routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "UserInput", id = UrlParameter.Optional }
);
        }
    }
}