﻿// <copyright file="IGetFizzBuzzPattern.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
////This interface accepts a number as parameter and implemented the same in GetfizzBuzzPatern class  to return list of numbers in string format
namespace BusinessLayer.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// This interface accepts a number as parameter and implemented the same in respective class.
    /// </summary>
    public interface IGetFizzBuzzPattern
    {
        /// <summary>
        /// This method is used to hold the list  of numbers starting from one
        /// </summary>
        /// <param name="userinputtedvalue">user inputted value</param>
        /// <returns>List of strings</returns>
        List<string> GetFizzBuzzPatterns(int userinputtedvalue);
    }
}
