﻿// <copyright file="ICommonDivisibleInterface.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This interface contains a common method declaration and implemented the same in divisible class file.
namespace BusinessLayer.Interfaces
{
    using System;

    /// <summary>
    /// This interface contains a common method declaration and implemented the same in other divisible class file.
    /// </summary>
    public interface ICommonDivisibleInterface
    {
        /// <summary>
        /// This method is used to check divisible logic
        /// </summary>
        /// <param name="number">user inputted number</param>
        /// <param name="day">day</param>
        /// <returns>string</returns>
        string IsDivisibleByNumber(int number, DayOfWeek day);
    }
}
