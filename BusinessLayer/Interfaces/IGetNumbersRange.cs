﻿using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
   public interface IGetNumbersRange
    {
        List<string> GetNumbersRangeFromOne(int lastindexedvalue);
    }
}
