﻿// <copyright file="IGetDayInterface.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
////This interface method used to get the day of the week
namespace BusinessLayer.Interfaces
{
    using System;

    /// <summary>
    /// This interface method accepts date time as parameter
    /// </summary>
    public interface IGetDayInterface
    {
        /// <summary>
        /// This method is used to return the day of the week
        /// </summary>
        /// <param name="datetime">date time</param>
        /// <returns>Day of the week</returns>
        DayOfWeek GetDayFromDate(DateTime datetime);
    }
}
