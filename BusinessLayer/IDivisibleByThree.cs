﻿
namespace BusinessLayer
{
    interface IDivisibleByThree
    {
        bool IsDivisibleByThree(int? number,out string  text);
    }
}
