﻿// <copyright file="DivisibleByThreeAndFiveLogic.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a logic to check the entered number is divisible by ThreeAndFive
////  This class also contains a logic to display a text if it's divisible by ThreeAndFive
namespace BusinessLayer
{
    using System;
    using Interfaces;
    
    /// <summary>
    /// This class contains a logic to check the entered number is divisible by ThreeAndFive
    ///  This class also contains a logic to display a text if it's divisible by ThreeAndFive
    /// </summary>
    public class DivisibleByThreeAndFiveLogic : ICommonDivisibleInterface
    {
        /// <summary>
        /// This method contains a business logic to check number is divisible by ThreeAndFive
        /// </summary>
        /// <param name="number">user inputted number</param>
        /// <param name="day">day</param>
        /// <returns>string</returns>
        public string IsDivisibleByNumber(int number, DayOfWeek day)
        {
            if ((number % 3 == 0) && (number % 5 == 0))
            {
                return Constants.Constants.StrFizzBuzzText;
            }

            return string.Empty;
        }
    }
}
