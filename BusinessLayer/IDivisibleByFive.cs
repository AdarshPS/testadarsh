﻿
namespace BusinessLayer
{
    interface IDivisibleByFive
    {
        bool IsDivisibleByFive(int? number, out string text);
    }
}
