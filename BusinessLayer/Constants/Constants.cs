﻿// <copyright file="Constants.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
////This file is used to declare a constants
namespace BusinessLayer.Constants
{
    /// <summary>
    /// This class can be used to create a constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// buzz text to display if number is divisible by 5 
        /// </summary>
        public const string StrBuzzText = "buzz";

        /// <summary>
        /// Text to display if number is divisible by 5 and day is wednesday
        /// </summary>
        public const string StrBuzzTxtForWednesday = "wuzz";

        /// <summary>
        /// fizz buzz text to display if number is divisible by 5 ands 3
        /// </summary>
        public const string StrFizzBuzzText = "fizz buzz";

        /// <summary>
        /// fizz text to display if number is divisible by 5
        /// </summary>
        public const string StrFizzText = "fizz";

        /// <summary>
        /// buzz text to display if number is divisible by 5 and day is wednesday
        /// </summary>
        public const string StrFizzTxtForWednesday = "wizz";
    }
}
