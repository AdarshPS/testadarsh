﻿
namespace BusinessLayer
{
    interface IDivisibleByThreeAndFive
    {
        bool IsDivisibleByThreeAndFive(int? number,out string text);       
    }
}
