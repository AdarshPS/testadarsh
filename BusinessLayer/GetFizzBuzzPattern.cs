﻿// <copyright file="GetFizzBuzzPattern.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
////This class will implements the interface method and returns list of numbers
////This class will implements the constrcutor injection
namespace BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// This class will implements the interface method and returns list of numbers
    /// </summary>
    public class GetFizzBuzzPattern : IGetFizzBuzzPattern
    {
        /// <summary>
        /// This interface array variable helps in constructor injection
        /// </summary>
        private readonly ICommonDivisibleInterface[] fizzBuzzBase;

        /// <summary>
        /// This is an Constructor injection
        /// </summary>
        /// <param name="fizzBuzzBase">fizzBuzzBase</param>
        public GetFizzBuzzPattern(ICommonDivisibleInterface[] fizzBuzzBase)
        {
            this.fizzBuzzBase = fizzBuzzBase;
        }

        /// <summary>
        /// This class will generate the list of numbers
        /// </summary>
        /// <param name="inputvalue">input value</param>
        /// <returns>list of numbers as string</returns>
        public List<string> GetFizzBuzzPatterns(int inputvalue)
        {
            int i;
            List<string> text = new List<string>();
            for (i = 1; i <= inputvalue; i++)
            {
                var result = this.fizzBuzzBase.ToList().Select(m => m.IsDivisibleByNumber(i, DateTime.Now.DayOfWeek)).ToList();
                text.Add(result.All(string.IsNullOrWhiteSpace)
                    ? i.ToString()
                    : result.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().FirstOrDefault());
            }

            return text;
        }
    }
}
