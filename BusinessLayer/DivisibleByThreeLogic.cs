﻿// <copyright file="DivisibleByThreeLogic.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a logic to check the entered number is divisible by Three
////  This class also contains a logic to display a text if it's divisible by Three
namespace BusinessLayer
{
    using System;
    using Interfaces;

    /// <summary>
    /// This class contains a logic to check the entered number is divisible by Three
    ///  This class also contains a logic to display a text if it's divisible by Three
    /// </summary>
    public class DivisibleByThreeLogic : ICommonDivisibleInterface
    {
        /// <summary>
        /// This method contains a business logic to check number is divisible by Three
        /// </summary>
        /// <param name="number">user inputted number</param>
        /// <param name="day">day</param>
        /// <returns>True or False</returns>
        public string IsDivisibleByNumber(int number, DayOfWeek day)
        {
            if (number % 3 == 0)
            {
                var returntext = day != DayOfWeek.Wednesday ? Constants.Constants.StrFizzText : Constants.Constants.StrFizzTxtForWednesday;
                return returntext;
            }

            return string.Empty;
        }
    }
}
