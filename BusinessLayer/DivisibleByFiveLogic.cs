﻿//// <copyright file="DivisibleByFiveLogic.cs" company="CompanyName">
////  Company copyright tag.
// </copyright>
////This class contains a logic to check the entered number is divisible by Five
////This class also contains a logic to display a text if it's divisible by five
namespace BusinessLayer
{
    using System;
    using Interfaces;

    /// <summary>
    /// This class contains a logic to check the entered number is divisible by Five
    ///  This class also contains a logic to display a text if it's divisible by five
    /// </summary>
    public class DivisibleByFiveLogic : ICommonDivisibleInterface
    {
        /// <summary>
        /// This method contains a business logic to check number is divisible by Five
        /// </summary>
        /// <param name="number">user inputted number</param>
        /// <param name="day">day</param>
        /// <returns>string</returns>
        public string IsDivisibleByNumber(int number, DayOfWeek day)
        {
            if (number % 5 == 0)
            {
                var returntext = day != DayOfWeek.Wednesday ? Constants.Constants.StrBuzzText : Constants.Constants.StrBuzzTxtForWednesday;
                return returntext;
            }

            return string.Empty;
        }
    }
}
