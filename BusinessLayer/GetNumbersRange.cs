﻿using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Interfaces;


namespace BusinessLayer
{
    /// <summary>
    /// This class is used to generate the number range from one to user inputted value.
    /// </summary>
    public class GetNumbersRange : IGetNumbersRange
    {
        private readonly IGetFizzBuzzPattern _iGetFizzBuzzPattern;
        //
        public GetNumbersRange(IGetFizzBuzzPattern iGetFizzBuzzPattern)
        {
            _iGetFizzBuzzPattern = iGetFizzBuzzPattern;
        }
        /// <summary>
        /// This method accepts the entered value as parameter and send the list of numbers to the class file to generate a patterns
        /// </summary>
        /// <param name="userinputtedvalue"></param>
        /// <returns>list of strings</returns>
        public List<string> GetNumbersRangeFromOne(int userinputtedvalue)
        {
            var listofNumbers = Enumerable.Range(1, userinputtedvalue).ToList();
            var result = _iGetFizzBuzzPattern.GetFizzBuzzPatterns(listofNumbers);
            return result;
        }
    }
}
