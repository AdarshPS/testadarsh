﻿// <copyright file="DivisibleByThreeLogicUnitTestCase.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
namespace FizzBuzzUnitTest
{
    using System;
    using BusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeLogicUnitTestCase
    {
        /// <summary>
        /// This Test method will call the respective method present in Business Layer
        /// </summary>
        [Test]
        public void DivisibleByThreeLogic()
        {
            var obj = new DivisibleByThreeLogic();
            string result = obj.IsDivisibleByNumber(3, DayOfWeek.Monday);
            Assert.AreEqual(result, "fizz");
        }

        /// <summary>
        /// Divisible logic for wednesday
        /// </summary>
        [Test]
        public void DivisibleByThreeLogicforWednesday()
        {
            var divthreelogicforwednesday = new DivisibleByThreeLogic();
            string result = divthreelogicforwednesday.IsDivisibleByNumber(6, DayOfWeek.Wednesday);
            Assert.AreEqual(result, "wizz");
        }

        /// <summary>
        /// This Test method will call the respective method present in Business Layer 
        /// </summary>
        [Test]
        public void NotDivisibleByThreeLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1, DayOfWeek.Monday);
            Assert.AreEqual(result, string.Empty);
        }
    }
}
