﻿using NUnit.Framework;
using BusinessLayer;

namespace FizzBuzzUnitTest
{
    /// <summary>
    /// Author: Adarsh P S
    /// This class contains a unit test cases for the method present in the Business layer.
    /// </summary>
    [TestFixture]
    public class FizzBuzzTest
    {

        [Test]
        public void DivisibleByFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(5);
            Assert.AreEqual(result, "buzz");
        }
        [Test]
        public void DivisibleByFiveLogicforWednesday()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(5);
            Assert.AreEqual(result, "wuzz");
        }


        [Test]
        public void DivisibleByThreeLogic()
        {
            var obj = new DivisibleByThreeLogic();
            string result = obj.IsDivisibleByNumber(3);
            Assert.AreEqual(result, "fizz");
        }
        [Test]
        public void DivisibleByThreeLogicForWednesday()
        {
            var obj = new DivisibleByThreeLogic();
            string result = obj.IsDivisibleByNumber(3);
            Assert.AreEqual(result, "wizz");
        }
        [Test]
        public void DivisibleByThreeAndFiveLogic()
        {
            var obj = new DivisibleByThreeAndFiveLogic();
            string result = obj.IsDivisibleByNumber(15);
            Assert.AreEqual(result, "fizz buzz");
        }

        [Test]
        public void NotDivisibleByFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1);
            Assert.AreEqual(result, string.Empty);
        }
        [Test]
        public void NotDivisibleByThreeAndFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1);
            Assert.AreEqual(result, string.Empty);
        }
        [Test]
        public void NotDivisibleByThreeLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1);
            Assert.AreEqual(result, string.Empty);
        }


    }
}

