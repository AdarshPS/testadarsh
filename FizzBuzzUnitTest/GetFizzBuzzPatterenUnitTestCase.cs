﻿// <copyright file="GetFizzBuzzPatterenUnitTestCase.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a unit test cases for the method present in the Business layer GetFizzBuzzPattern class.
namespace FizzBuzzUnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using BusinessLayer.Interfaces;
    using FizzBuzzTestApp.Controllers;
    using FizzBuzzTestApp.Models;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// This class contains a unit test cases for the method present in the Business layer.
    /// </summary>
    [TestFixture]
    public class GetFizzBuzzPatterenUnitTestCase
    {
        /// <summary> 
        /// Mocking interface
        /// </summary>
        private Mock<IGetFizzBuzzPattern> igetFizzBuzzPattern;

        /// <summary>
        /// This method helps to set up the necessary rules to do a mock for the controller method
        /// </summary>
        [SetUp]
        public void GetFizzBuzzPatternTestFixtureSetUp()
        {
            List<string> llstnumbers = new List<string> { "1", "2", "fizz", "4" };

            int number = 4;
            this.igetFizzBuzzPattern = new Mock<IGetFizzBuzzPattern>();
            this.igetFizzBuzzPattern.Setup(x => x.GetFizzBuzzPatterns(number)).Returns(llstnumbers);
        }

        /// <summary>
        /// This test case method helps in mocking constructor injection 
        /// </summary>
        [Test]
        public void GetFizzBuzzPattern()
        {
            int number = 4;
            this.GetFizzBuzzPatternTestFixtureSetUp();
            var controller = new FizzBuzzController(this.igetFizzBuzzPattern.Object);
            ViewResult result = controller.UserInput(Convert.ToString(number)) as ViewResult;
            if (result != null)
            {
                if (result.ViewData != null)
                {
                    var model = result.ViewData.Model as FizzBuzzModel;
                    Assert.True(result.ViewData.ModelState.IsValid);
                }
            }
        }
    }
}