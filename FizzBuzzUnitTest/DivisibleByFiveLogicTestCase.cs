﻿// <copyright file="DivisibleByFiveLogicTestCase.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
namespace FizzBuzzUnitTest
{
    using System;
    using BusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
    /// </summary>
    [TestFixture]
    public class DivisibleByFiveLogicTestCase
    {
        /// <summary>
        /// This Test method will call the respective method present in Business Layer
        /// </summary>
        [Test]
        public void DivisibleByFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(5, DayOfWeek.Monday);
            Assert.AreEqual(result, "buzz");
        }

        /// <summary>
        /// This Test method will call the respective method present in Business Layer 
        /// </summary>
        [Test]
        public void NotDivisibleByFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1, DayOfWeek.Monday);
            Assert.AreEqual(result, string.Empty);
        }

        /// <summary>
        /// Divisible logic for wednesday
        /// </summary>
        [Test]
        public void DivisibleByFiveLogicforWednesday()
        {
            var divfivelogicforwednesday = new DivisibleByFiveLogic();
            string result = divfivelogicforwednesday.IsDivisibleByNumber(5, DayOfWeek.Wednesday);
            Assert.AreEqual(result, "wuzz");
        }
    }
}
