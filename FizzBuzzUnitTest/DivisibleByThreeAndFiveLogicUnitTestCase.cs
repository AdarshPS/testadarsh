﻿// <copyright file="DivisibleByThreeAndFiveLogicUnitTestCase.cs" company="CompanyName">
//  Company copyright tag.
// </copyright>
//// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
namespace FizzBuzzUnitTest
{
    using System;
    using BusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// This class contains a unit test cases for the method present in the DivisibleByThreeAndFiveLogic Class file
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeAndFiveLogicUnitTestCase
    {
        /// <summary>
        /// This Test method will call the respective method present in Business Layer
        /// </summary>
        [Test]
        public void DivisibleByThreeAndFiveLogic()
        {
            var obj = new DivisibleByThreeAndFiveLogic();
            string result = obj.IsDivisibleByNumber(15, DayOfWeek.Monday);
            Assert.AreEqual(result, "fizz buzz");
        }

        /// <summary>
        /// This Test method will call the respective method present in Business Layer 
        /// </summary>
        [Test]
        public void NotDivisibleByThreeAndFiveLogic()
        {
            var obj = new DivisibleByFiveLogic();
            string result = obj.IsDivisibleByNumber(1, DayOfWeek.Monday);
            Assert.AreEqual(result, string.Empty);
        }
    }
}
